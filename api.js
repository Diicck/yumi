// api.js
import API_HOST from "@/api-host";

export const login = (username, password) => {
    return new Promise((resolve, reject) => {
        uni.request({
            url: `${API_HOST}front/auth/loginByUserName`,
            method: 'POST',
            header: {
                "Content-Type": "application/json",
                "Accept": "*/*",
                "Host": "10.2.34.10:10006",
                "Connection": "keep-alive"
            },
            data: {
                phone: username,
                password: password
            },
            success: (res) => {
                resolve(res.data);
            },
            fail: (err) => {
                reject(err);
            }
        });
    });
};
